# OmekaApiBrowser

Module pour [ProcessWire](https://processwire.com/) 3.0.

![Capture d'un champ OmekaApiBrowser](assets/img/OmekaApiBrowser.PNG)

Stocke un api host Omeka, une portion de requête sur les pages et le résultat d'une requête en JSON.

- crée un InputfieldWrapper dans la config du module qui contient `$api_host`, `$api_request`, `$api_response`
- récupère `$api_host` dans la config du module
- stocke `$api_request` dans les pages
- stocke `$api_response` dans les pages

Sur la page config du module, la checkbox "Mettre à jour tous les champs" ($update_all_fields) sert à mettre à jour d'un seul coup tous les champs utilisant le fieldtype du module (sur toutes les pages même s'ils y sont multiples). Il faut cocher la checkbox et sauvegarder la page.

Développé dans le cadre du [CDP Performance Laboratory](https://performance.univ-grenoble-alpes.fr/).

## Installation

[Voir la doc](https://processwire.com/docs/modules/intro/)

- placer le dossier dans 'site/modules/'
- dans l'admin, aller dans 'Modules > Install'
- installer 'OmekaApiBrowser'

Dans l'admin, dans les champs de configuration de OmekaApiBrowser, remplir selon l'API host disponible.

Le plugin est installé, il faut maintenant créer un champ et l'ajouter dans un template : 
- Aller dans 'Setup > Fields > Add new'
- Nommer le champ, par exemple : 'omeka_browser'
- Sélectionner le type de champ créer par le module : 'OmekaApiBrowser'
- Ajouter ce champ sur un template de page
- Se rendre sur une page utilisant ce template de page

La page comporte deux inputs supplémentaires : "Requête API Omeka" et "Réponse API Omeka".

- remplir "Requête API Omeka" par une requête à l'API Omeka. Par exemple : `items/23` pour chercher l'item 23 de la base.
- cliquer sur "Parcourir API Omeka"

Un fetch JS est envoyé à l'API, le résultat est ajouté à l'input "Réponse API Omeka".

Pour la construction de requêtes à Omeka : [https://omeka.org/s/docs/developer/api/](https://omeka.org/s/docs/developer/api/)

La réponse de l'API est accessible dans les templates en front par : `$page->omeka_browser->api_response` cf. [méthodes de ProcessWire](https://processwire.com/docs/start/templates/#basic-usage).

## License

GNU Affero General Public License v3.0

---

[![Logo Performance Laboratory](assets/img/logos/logo-pl.jpg "Lien vers le site web du Performance Laboratory")](https://performance.univ-grenoble-alpes.fr/)

[![Logo UGA](assets/img/logos/logo-uga.jpg "Lien vers le site web de l'UGA")](https://www.univ-grenoble-alpes.fr/)